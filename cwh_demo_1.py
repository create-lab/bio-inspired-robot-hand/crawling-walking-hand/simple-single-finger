from comms_wrapper import *

key = Key()

# TODO: change portName to that on your computer
arduino1 = Arduino(descriptiveDeviceName="Arduino", portName="COM21", baudrate=115200)
arduino1.connect_and_handshake(verbose=True)

while 1:
    if key.keyPress == "1":
        arduino1.send_message("m1_fwd")
    elif key.keyPress == "2":
        arduino1.send_message("m1_rev")
    elif key.keyPress == "3":
        arduino1.send_message("m2_fwd")
    elif key.keyPress == "4":
        arduino1.send_message("m2_rev")
    else:
        arduino1.send_message("stop")