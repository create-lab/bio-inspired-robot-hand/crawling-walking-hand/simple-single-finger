#define m1a 2
#define m1b 3

#define m2a 4
#define m2b 5

#include "pyCommsLib.h"

void setup() {
  Serial.begin(115200);
  init_python_communication();
  
  pinMode(m1a, OUTPUT);
  pinMode(m1b, OUTPUT);

  pinMode(m2a, OUTPUT);
  pinMode(m2b, OUTPUT);

}

void loop() {
  String msg = latest_received_msg();
  if(msg == "m1_fwd") {
    move_m1(true);
  }
  else if(msg == "m1_rev") {
    move_m1(false);
  }
  else if(msg == "m2_fwd") {
    move_m2(true);
  }
  else if(msg == "m2_rev") {
    move_m2(false);
  }
  else {
    stop_both_m();
  }

  sync();
}

void move_m1(bool dir) {
  if(dir == true) {
    analogWrite(m1a, 255);
    digitalWrite(m1b, LOW);
  }
  else {
    digitalWrite(m1a, LOW);
    analogWrite(m1b, 255);
  }
}

void move_m2(bool dir) {
  if(dir == true) {
    analogWrite(m2a, 255);
    digitalWrite(m2b, LOW);
  }
  else {
    digitalWrite(m2a, LOW);
    analogWrite(m2b, 255);
  }
}

void stop_both_m() { 
  digitalWrite(m2a, LOW);
  digitalWrite(m2b, LOW);
  digitalWrite(m1a, LOW);
  digitalWrite(m1b, LOW);
}